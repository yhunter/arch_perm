jQuery(document).ready(function() {

    $.featherlightGallery.prototype.afterContent = function() {
		  var caption = this.$currentTarget.find('img').attr('title');
		  this.$instance.find('.caption').remove();
		  $('<div class="caption">').appendTo(this.$instance.find('.featherlight-content'));
		  $('<div>').text(caption).appendTo(this.$instance.find('.caption'));
    };
    

});

var $window = $(window);
var $header = $('.sticky-header');
var scrolling = false;
var previousTop = 0;
var scrollDelta = 10;
var scrollOffset = 250; 

$window.on('scroll', function() {
  if (!scrolling) {
    scrolling = true;

    if (!window.requestAnimationFrame) {
      setTimeout(autoHideHeader, 250);
    } else {
      requestAnimationFrame(autoHideHeader);
    }
  }
});

function autoHideHeader() {
  var currentTop = $window.scrollTop();

  if (currentTop < 10) {
    // Is at top
    $header.removeClass('is-hidden');
  } else if (previousTop - currentTop > scrollDelta) {
    // Scrolling up
    $header.removeClass('is-hidden');
  } else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
    // Scrolling down
    $header.addClass('is-hidden');
  }

  previousTop = currentTop;
  scrolling = false;
}

function scrollTo(anchor) {

    if (anchor.match(/^\/#.*/g)){ 
      anchor = anchor.replace("\/#", "#");
      if ($(anchor).length>0) {
          var top = jQuery(anchor).offset().top;
    
          top -= 60; // so it's not directly at the top of the screen being overlapped by the nav
      
          jQuery('html,body').animate({scrollTop: top}, 1500);
      
          return false; // scroll to, cancel default link behavior
      }
      
    }
  
    return true; // follow link
  }
  
  //Переход по фильтрам
  jQuery(document).ready(function(){
    let urlToGo;
    let nomination = 0;
    let year = 0;
    $("#filters #filters_show").on( "click", function() {
      urlToGo = new URL(window.location.href);
      nomination = $('#filters #nomination').val();
      year = $('#filters #year').val();
      
      urlToGo.searchParams.set('nomination', nomination);
      
      urlToGo.searchParams.set('y', year);

      document.location.href = urlToGo;
    });
  });

