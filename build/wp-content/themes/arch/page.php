<?php get_header();?>




<div class="teaser project">
    <div class="wrapper">
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // Начало цикла ?>
        <div class="col12">
            <h1><?php the_title(); // Заголовок ?></h1>
        </div>
        <div class="col8 project__content">

            <?php the_content(); // Содержимое страницы ?>

        </div>
        <div class="col4 project__details">

        </div>
        <?php endwhile; // Конец цикла ?>
    </div>
</div>


<?php get_footer(); // Подключаем футер ?>



