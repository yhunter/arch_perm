<footer>
	<div class="wrapper">
		<div class="col7 copyright">
			Пермская краевая общественная организация «Союз&nbsp;Архитекторов»,
			

			2020—<?php echo date("Y"); ?>
<br />
			Эл. почта: <a href="mailto:souz-arhitektorov@mail.ru">souz-arhitektorov@mail.ru</a>
		</div>
		<div class="col5 design">
			<a href="https://www.senator-perm.ru">Создание сайта: Имиджевый&nbsp;центр&nbsp;«<span>Сенатор</span>»,
				2020</a>
		</div>
	</div>
</footer>
</div>




<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/main.min.css?rev=0.7603078532413543">
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.11.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?rev=0.7603078532413543"></script>
<?php
/**
 * Чистый Шаблон для разработки
 * Шаблон футера
 * http://dontforget.pro
 * @package WordPress
 * @subpackage clean
 */

	wp_footer(); // Необходимо для нормальной работы плагинов
?>


</body>

</html>