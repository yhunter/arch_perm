<!DOCTYPE html>
<html lang="ru">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400&display=swap"
		rel="stylesheet">
	<!-- RSS, стили -->
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>" />
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/rss+xml" title="Comments RSS"
		href="<?php bloginfo('comments_rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


	<style>
		<?php include (TEMPLATEPATH . '/_inline.php');
		?>


	

	* {}
	</style>


	<title>
		<?php // Генерируем тайтл в зависимости от контента с разделителем " | "
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
?>

	</title>


	<?php
	wp_head(); // Необходимо для работы плагинов и функционала wp
?>


</head>

<body>
	<?php include (TEMPLATEPATH . '/symbol.php'); ?>


	


	<nav
		class="FixedNav header  sticky-header <?php if(is_front_page()) echo 'front-menu';?>">

		<div class="menucol6 header__left">
			<div class="logo">
				<a href="/" title="На главную"><img src="/wp-content/themes/arch/img/arch-perm-logo.png" alt=""></a>
			</div>
			<h1>ПКОО «Союз архитекторов»</h1>
		</div>
		<a id="touch-menu" class="mobile-menu" href="#"><svg viewBox="0 0 100 100" class="nav-icon">
				<use xlink:href="#menu"></use>
			</svg></a>
		<div class="<?php if(!is_front_page()) echo 'menucolfull';?> header__menu">

			<?php
		$args = array( // Выводим верхнее меню
		'menu'=>'Верхнее меню',
		'container'=>'',
		'depth'=> 0);
		wp_nav_menu($args);
?>


			


		</div>

	</nav>

	<div class="page grey">