<?php /* Template Name: Contest Page */ ?><?php get_header();?>



<div class="gray">

    <div class="teaser" id="about">
        <div class="wrapper">
            <div class="col5 teaser__image colRight">
                <?php if ( has_post_thumbnail() ) { 
                        
                        $large_image_url = wp_get_attachment_image_url( get_post_thumbnail_id(), 'full');
                        
                        echo '<img src="'.$large_image_url.'" alt="" />';
                        echo '<h3>'.get_the_title().'</h3>';
                    }  ?>

                

            </div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); // Цикл записей ?>
            <div class="col7">


                <?php the_content(''); // Выводим анонс ?>
                <?php endwhile; // Конец цикла.
                else: echo '<h2>Извините, ничего не найдено...</h2>'; endif; // Если записей нет - извиняемся ?>



                <div class="contest-menu">
                    <?php
                            $args = array( // Выводим верхнее меню
                            'menu'=>'Меню конкурса',
                            'container'=>'',
                            'depth'=> 0);
                            wp_nav_menu($args);
                    ?>
                </div>
            </div>

            


        </div>
    </div>

    <div id="sponsor" class="sponsor">
        <a href="https://www.knauf.ru/"><img src="/wp-content/themes/arch/img/knauf.svg" alt="knauf" /></a>
        <p>Генеральный партнёр «Союза архитекторов»</p>
    </div>

    <div class="wrapper">


        <div class="cards" id="works">
            <h2>Работы участников</h2>
            <?php 

                    $mainCatCollection = explode(',', get_field('contest-cat-IDs', 523)); //[14,15,7,8,9,10,11];
                    foreach ($mainCatCollection AS $index => $value)
                    $mainCatCollection[$index] = (int)$value; 
                
                    $specialCat = explode(',', get_field('special-cat-ID', 523));
                    foreach ($specialCat AS $index => $value)
                    $specialCat[$index] = (int)$value; 

                    termCard($mainCatCollection);
            ?>


        </div>
        <div class="cards" id="special" style="padding-top: 0">
            <h3>Специальная номинация</h3>
            <?php 

                    termCard($specialCat);
            ?>
        </div>

        <div class="cards" id="special" style="padding-top: 0">
            <a href="/projects/">Архив работ</a>
        </div>

        <?php if(get_field('отображать_победителей') ) : ?>
        <?php if( get_field( "отображать_победителей", $post_id ) ): ?>
        <div class="cards" id="winners">
            <h2>Галерея лауреатов</h2>
            <?php 
                $year = lastWinnersYear();   //Получаем год последнего поста в категории победителей                             
                winCard($mainCatCollection, $year);
            ?>
        </div>
        <?php endif; ?>
        <?php endif; ?>
        <?php if(get_field('отображать_победителей') ) : ?>
        <?php if( get_field( "отображать_победителей", $post_id ) ): ?>
        <div class="cards" id="winnersSpecial">
            <h3>Специальная номинация</h3>
            <?php 

                winCard($specialCat, $year);
            ?>
        </div>
        <?php endif; ?>
        <?php endif; ?>

        


        <div class="cards" id="special" style="padding-top: 0">
            <a href="/projects/">Архив работ</a>
        </div>
        <?php if(get_field('jury', $post_id) ) : ?>
        <div class="jury">
            <?php the_field("jury", $post_id); ?>
        </div>
        <?php endif; ?>

        


        <div class="form" id="form">
            <h2>Подать заявку</h2>
            <div class="col6 centered">
                <?php if(get_field('Принимаем заявки') ) : ?>
                <?php if( get_field( "Подать заявку", $post_id ) ): ?>

                <?php the_field( "Подать заявку", $post_id ); ?>
                <?php endif; ?>
                <?php else :?>
                <p style="text-align: center; color: #666">Прием заявок закрыт.</p>
                <?php endif; ?>

                

            </div>
        </div>
    </div>

</div>



<?php get_footer(); // Подключаем футер ?>



