<?php get_header();?>



<div class="cards projects project-archive" id="workscategory">
    <h2>Архив конкурса</h2>
    <div id="filters">
        <select name="nomination" id="nomination">
            <option value="0">Все номинации</option>



            <?php 
    $CheckFestYear=0;
    $yearsCollection = [];
    $showSpecial = true;
    $getNom = 0;
    $getYear = 0;

    if(isset($_GET["nomination"])){
        $getNom = (int)$_GET["nomination"];
    } 

    if(isset($_GET["y"])){
        $getYear = (int)$_GET["y"];
    } 
    

    $mainCatCollection = explode(',', get_field('contest-cat-IDs', 523)); //[14,15,7,8,9,10,11]; 
    
    

    foreach ($mainCatCollection AS $index => $value) {
        $selected = "";
        if($getNom != 0){
            if ($getNom == (int)$value) $selected = 'selected="selected"';
        } 
        $mainCatCollection[$index] = (int)$value; 
        $tax = get_term( $value );
        echo '<option '.$selected.' value="'.$value.'">'.$tax->name.'</option>';        
    }    

    $specialCat = (int)get_field('special-cat-ID', 523);
    $tax = get_term( $specialCat );
    $selected = "";

    if($getNom != 0){
        
        if ($getNom == (int)$specialCat) $selected = 'selected="selected"';
    } 
    echo '<option '.$selected.'  value="'.$specialCat.'">'.$tax->name.'</option>'; 

    if($getNom != 0) {
        $mainCatCollection = [];
        $mainCatCollection[] = $_GET["nomination"];
        $showSpecial = false;
    }

    ?>
        </select>

        <?php    
    
    if (have_posts()) {
            while (have_posts()) {
                the_post(); // Цикл записей
                $year = the_date('Y', '', '', FALSE);
		        $FestYear = $year;    
		        if (($FestYear<>$CheckFestYear)and($FestYear<>'')) {$CheckFestYear=$FestYear; $yearsCollection[] = $FestYear;}
            }            
    }

    
       
								
	?>
        <select name="year" id="year">
            <option value="0">Все годы</option>
            <?php 
                foreach ($yearsCollection as &$currentyear) {
                    $selected = '';
                    if($getYear != 0){
                        if ($getYear == (int)$currentyear) $selected = 'selected="selected"';
                    } 
                    echo '<option '.$selected.' value="'.$currentyear.'">'.$currentyear.'</option>';
                }
            ?>

        </select>
        <button id="filters_show" class="btn-ft-red">Показать</button>
    </div>

    <?php 
        if($getYear != 0){
            $yearsCollection = [];
            $yearsCollection[] = $getYear;
        }
    ?>

    <?php 
        foreach ($yearsCollection as &$currentyear) {
            //echo $currentyear;
            foreach ($mainCatCollection as &$termCat) {
                 //echo $termCat;
                showArchiveCat($termCat, $currentyear);
            }
            if ($showSpecial) {
                showArchiveCat($specialCat, $currentyear);
            }                    
        }
    

     ?>




</div>


<?php get_footer(); // Подключаем футер ?>


