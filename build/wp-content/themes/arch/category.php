<?php get_header();?>




<div class="teaser project single news">
	<div class="wrapper">
		<div class="col12">
			<h1>Новости: <?php wp_title(''); // Заголовок категории ?></h1>
		</div>
		<?php 
		global $wp_query;
		$args = array_merge( $wp_query->query_vars, array( 'post_type' => 'news' ) ); //Добавляем новости
		$query = new WP_Query( $args ); ?>

		<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
		<div class="col8 single__content news_content">
			<div class="news_entry">
				<h3><a href="<?php the_permalink(); ?>"
						title="Перейти к новости  <?php the_title(); ?>"><?php the_title(); ?></a></h3>
				<div class="single__date">
					<?php the_time('F j, Y'); // Дата создания поста ?>
					<?php the_category(' '); ?>
				</div>
				<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php the_permalink(); ?>"
					title="Перейти к новости <?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
				<?php endif; ?>


				<?php the_content(); // Содержимое страницы ?>
			</div>

			<div class="navigator">

				<?php // Пагинация
                    global $wp_query;
                    $big = 999999999;
                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'type' => 'list',
                        'prev_text'    => __(''), 
                        'next_text'    => __(''),
                        'total' => $wp_query->max_num_pages
                    ) );
                    ?>


			</div>
		</div>
		<div class="col4 single__details">
			<b>Все рубрики новостей:</b>
			<ul>
				<?php 
			$categories = get_categories();foreach($categories as $category) {   echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';}
			?>
			</ul>

		</div>
		<?php endwhile; // Конец цикла.
		else: echo '<h2>Извините, ничего не найдено...</h2>'; endif; // Если записей нет - извиняемся ?>
	</div>
</div>






<?php get_footer(); // Подключаем футер ?>



