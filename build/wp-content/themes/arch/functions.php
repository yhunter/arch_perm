<?php
/**
 * Чистый Шаблон для разработки
 * Функции шаблона
 * http://dontforget.pro
 * @package WordPress
 * @subpackage clean
 */
register_nav_menus( array( // Регистрируем 2 меню
	'top' => 'Main menu'
) );
add_theme_support('post-thumbnails'); // Включаем поддержку миниатюр
set_post_thumbnail_size(320, 320); // Задаем размеры миниатюре

if ( function_exists('register_sidebar') )
register_sidebar(); // Регистрируем сайдбар

if(!is_admin()) {

	add_action( 'show_admin_bar', '__return_false' );
	
}

add_action('admin_menu', 'remove_admin_menu');
function remove_admin_menu() {
	//remove_menu_page('options-general.php'); // Удаляем раздел Настройки	
  	remove_menu_page('tools.php'); // Инструменты
	//remove_menu_page('users.php'); // Пользователи
	//remove_menu_page('plugins.php'); // Плагины
	//remove_menu_page('themes.php'); // Внешний вид	
	remove_menu_page('edit.php'); // Посты блога
	//remove_menu_page('upload.php'); // Медиабиблиотека
	//remove_menu_page('edit.php?post_type=page'); // Страницы
	remove_menu_page('edit-comments.php'); // Комментарии	
	remove_menu_page('link-manager.php'); // Ссылки
  	//remove_menu_page('wpcf7');   // Contact form 7

}

//Disable emoji support
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function termCard($termArray) {
	foreach ($termArray as &$value) {
                        // получим ID картинки из метаполя термина
                        $image_id = get_term_meta( $value, 'изображение_категории', 1 );
                        $taxname = get_term( $value );


                        // ссылка на полный размер картинки по ID вложения
                        $image_url = wp_get_attachment_image_url( $image_id, 'large' );
                        if ($image_url=='') $image_url='http://www.arch-perm.ru/wp-content/uploads/2020/09/daniel-von-appen-tb4heMa-ZRo-unsplash.jpg';
                        $term_link = get_term_link($value, '');

                        // выводим картинку на экран
                        echo "<div class='card' style='background-image: url(".$image_url.");'>";
                        echo '<a href="'.$term_link.'" title="'.$taxname->name.'" >';
                        echo '<div class="grad"><h3>'.$taxname->name.'</h3></div>';
                        echo '</a>';
                        echo '</div>';
    };
}

function lastWinnersYear() {  //Получаем год последнего поста в категории победителей   
	$year = null;
	$args = array(
			'posts_per_page' => 1,
			'post_type' => 'projects',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'nominations',
					'field'    => 'slug',
					'terms'    => array('winners'),
				)
			),
	);
		
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post(); 
			$year = the_date('Y', '', '', FALSE);
		} 	
	}
	wp_reset_query(); 
	return($year);

}

function winCard($termArray, $year = null) { //Выводим карточки победителей
	
	
	foreach ($termArray as &$value) {
		
		//$query = new WP_Query( [ ] );

		$args = array(
			'post_type' => 'projects',
			'year' => $year,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'nominations',
					'field'    => 'slug',
					'terms'    => array('winners'),
				),
				array(
					'taxonomy' => 'nominations',
					'field'    => 'term_id',
					'terms'    => array( $value ),
				),
				array(
			    'taxonomy' => 'nominations',
			    'terms'    => 16,
			    'operator' => 'NOT IN',
			   ),
			),
		);
		$query = new WP_Query( $args );


		while ( $query->have_posts() ) {
			$query->the_post();
			if( get_field( 'изображение_1') ) {
				$image_url=get_field( 'изображение_1');
			}
			else $image_url='';

			$term_link=get_permalink();
			echo "<div class='card win' style='background-image: url(".$image_url.");'>";
            echo '<a href="'.$term_link.'" title="'.$taxname->name.'" >';
            echo '<div class="grad"><h3>'.get_term( $value )->name.'</h3></div>';
            echo '</a>';
            echo '</div>';
			
			//the_title(); // выведем заголовок поста
		}
		wp_reset_query();
	}
}

function showArchiveCat($catId, $year) {
global $wp_query;
		




$args = array_merge(
		 $wp_query->query_vars, array('post_type' => 'projects', 'year' => $year, 'tax_query' => 
		 	array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'nominations',
					'field'    => 'term_id',
					'terms'    => $catId,
				),
			),));


//Выводим посты на последний добавленный год
$query = new WP_Query( $args );

if ( $query->have_posts() ) {

	 echo '<h3>';
			$tax = get_term( $catId );
			echo $tax->name.' – '.$year;
		echo '</h3>';   
 
    // Start looping over the query results.
    while ( $query->have_posts() ) {


 
        $query->the_post();

        if( get_field( 'изображение_1') ) {
				$image_url=get_field( 'изображение_1');
		}
		else $image_url='';

		$win = '';
		if( has_term( 'winners', 'nominations', $post->ID ) ) $win = 'win';

		$term_link=get_permalink();

        	echo "<div class='card ".$win."' style='background-image: url(".$image_url.");'>";
            echo '<a href="'.$term_link.'" title="'.$taxname->name.'" >';
            echo '<div class="grad"><h3>'.the_title('', '', 0).'</h3></div>';
            echo '</a>';
            echo '</div>';
 
        // Contents of the queried post results go here.
 
    }
 
}
wp_reset_query();

		

}

function main_query_mods( $query ) {
    // check http://codex.wordpress.org/Conditional_Tags to play with other queries

        $query->set('posts_per_page',1);

}
//add_action( 'pre_get_posts', 'main_query_mods' );




//ADDING AN ACTIVE CLASS TO THE CUSTOM POST-TYPE MENU ITEM WHEN VISITING ITS SINGLE POST PAGES
function custom_active_item_classes($classes = array(), $menu_item = false){            
	global $post;

	$classes[] = ($menu_item->url == get_post_type_archive_link($post->post_type)) ? 'current-menu-item active' : '';
	return $classes;
}
add_filter( 'nav_menu_css_class', 'custom_active_item_classes', 10, 2 );

function change_page_menu_classes($menu)
{
    global $post;
    if (get_post_type($post) == 'projects')
    {
        $menu = str_replace( 'current-menu-item', '', $menu ); // remove all current_page_parent classes
        $menu = str_replace( 'menu-item-630', 'menu-item-630 current-menu-item', $menu ); // add the current_page_parent class to the page you want
    }
    return $menu;
}
add_filter( 'nav_menu_css_class', 'change_page_menu_classes', 15 ,2 );



?>